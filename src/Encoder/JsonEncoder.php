<?php

namespace Workshop\Solid\Encoder;

class JsonEncoder
{
    /**
     * @param array $data
     *
     * @return string
     */
    public function encode(array $data)
    {
        return json_encode($data);
    }
}

