<?php

namespace Workshop\Solid;

use Workshop\Solid\Encoder\JsonEncoder;
use Workshop\Solid\Encoder\XmlEncoder;

class GenericEncoder
{
    /**
     * @param array  $data
     * @param string $format
     *
     * @return string
     */
    public function encodeToFormat(array $data, $format)
    {
        switch ($format) {
            case 'json':
                $encoder = new JsonEncoder();
                break;

            case 'xml':
                $encoder = new XmlEncoder();
                break;

            default:
                throw new \InvalidArgumentException(sprintf('Unknown format %s', $format));
        }

        $data = $this->prepareData($data, $format);

        return $encoder->encode($data);
    }

    /**
     * @param array  $data
     * @param string $format
     *
     * @return mixed
     */
    private function prepareData(array $data, $format)
    {
        switch ($format) {
            case 'json':
                $data = $this->forceArray($data);
                $data = $this->fixKeys($data);
                break;

            case 'xml':
                $data = $this->fixAttributes($data);
                break;

            default:
                throw new \InvalidArgumentException(sprintf('Unknown format %s', $format));
        }

        return $data;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function forceArray(array $data)
    {
        // ...

        return $data;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function fixKeys(array $data)
    {
        // ...

        return $data;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function fixAttributes(array $data)
    {
        // ...

        return $data;
    }
}
